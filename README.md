# 基于 Kubernetes 部署 EFK 平台

## 1、安装Helm 3.4.2
```shell
curl -O https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz
tar -zxvf helm-v3.4.2-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
helm repo add elastic https://helm.elastic.co
```
## 2、使用helm创建持久化存储卷
准备NFS存储服务器一台
|    IP地址   |    端口   |   服务   |
|-------------|:--------:|:--------:|
|192.168.1.100| 111,2029 |NFS Server|
```shell
# 安装 nfs-client-provisioner
helm install nfs-storage -n default \
--set nfs.server=192.168.1.100,nfs.path=/data/NFS \
--set storageClass.name=nfs-storage,storageClass.reclaimPolicy=Retain \
apphub/nfs-client-provisioner
```
- nfs.server：指定 nfs 服务地址
- nfs.path：指定 nfs 对应目录
- storageClass.name：此配置用于绑定 PVC 和 PV
- storageClass.reclaimPolicy：回收策略，默认是删除
- -n：命名空间

## 下载代码仓库
```
git clone https://gitlab.com/snight/efk.git
cd efk/efk-yaml
```

## 部署EFK集群
```shell
# 创建命名空间efk-system
kubectl crate -f efk-system.yaml

# 创建RBAC
kubectl create -f efk-rbac.yaml

# 创建主节点
kubectl create -f efk-master.yaml

# 创建数据节点
kubectl create -f efk-data.yaml

# 创建filebeat
kubectl create -f efk-filebeat.yaml

# 创建kibana
kubectl create -f efk-kibana.yaml

# 创建服务
kubectl create -f efk-service.yaml
```

## EFK集群服务端口
|   service   |port|nodePort|
|-------------|:--:|:------:|
|elasticsearch|9200| 30002  |
|kibana       |5601| 30003  |

